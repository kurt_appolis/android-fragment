package appolis.kurt.fragmentstemp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import appolis.kurt.fragmentstemp.R;

/**
 * Created by kurt on 2016/05/27.
 */
public class FragmentAltView
        extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view,
                container, false);
        setUpView(view);
        return view;
    }

    public void setUpView(View view) {
        //TODO - set up your view here
        TextView textview = (TextView) view.findViewById(R.id.textview);
        textview.setText("this is the 2nd fragment");
    }
}
