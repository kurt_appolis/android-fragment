package appolis.kurt.fragmentstemp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import appolis.kurt.fragmentstemp.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
